# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/shortwave/issues
# Bug-Submit: https://github.com/<user>/shortwave/issues/new
# Changelog: https://github.com/<user>/shortwave/blob/master/CHANGES
# Documentation: https://github.com/<user>/shortwave/wiki
# Repository-Browse: https://github.com/<user>/shortwave
# Repository: https://github.com/<user>/shortwave.git
